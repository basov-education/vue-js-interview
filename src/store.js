import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    users: [],
    filters: {},
    isLoading: false,
  },
  getters: {
    getUsers: (state) => {
      return state.users.filter(
        (user) =>
          (!state.filters.country || state.filters.country === user.country) &&
          (!state.filters.score ||
            (state.filters.score === "< 10" && user.score < 10) ||
            (state.filters.score === "> 20" && user.score > 20))
      );
    },
    getLoading: (state) => state.isLoading,
  },
  actions: {
    addUsers({ commit }, users) {
      commit("SET_LOADING", true);
      
      setTimeout(() => {
        commit("ADD_USERS", users);
        commit("SET_LOADING", false);
      }, 2000);
    },
    updateFilters({ commit }, filters) {
      commit("SET_LOADING", true);
      // TODO: AbortSignal должен быть
      setTimeout(() => {
        commit("UPDATE_FILTERS", filters);
        commit("SET_LOADING", false);
      }, 2000);
    },
  },
  mutations: {
    ADD_USERS(state, users) {
        state.users = state.users.concat(users);
    },
    UPDATE_FILTERS(state, filters) {
      state.filters = filters;
    },
    SET_LOADING(state, isLoading) {
        state.isLoading = isLoading;
    }
  },
});
